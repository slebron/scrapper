package com.minexsoft.extractor.model;

/**
 * @author stainley lebron
 * @date on 3/11/2017.
 */
public class Fuel {
    private String name;
    private String value;
    private String dateToUpdate;

    public Fuel() {
    }

    public Fuel(String name, String value){
        this.name = name;
        this.value = value;
    }

    public Fuel(String name, String value, String dateToUpdate) {
        this.name = name;
        this.value = value;
        this.dateToUpdate = dateToUpdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDateToUpdate() {
        return dateToUpdate;
    }

    public void setDateToUpdate(String dateToUpdate) {
        this.dateToUpdate = dateToUpdate;
    }
}
