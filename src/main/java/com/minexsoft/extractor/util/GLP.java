package com.minexsoft.extractor.util;

/**
 * @author stainley lebron
 * @date 3/11/2017.
 */
public enum GLP {
    GAS_NATURAL("GasNatural"), GAS_DIESEL("GasDiesel"), GAS_PREMIUM("GasPremium"), BUY_DOLLARS("ExchangeDolarBuy"), GAS_PROPANO("GasPropane"), GAS_DIESEL_REG("GasDieselr"), GAS_REGULAR("GasRegular");

    GLP(String name) {
        this.name = name;
    }

    private String name;

    public String getValue() {
        return name;
    }
}
