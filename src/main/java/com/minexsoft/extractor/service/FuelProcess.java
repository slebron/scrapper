package com.minexsoft.extractor.service;

import com.minexsoft.extractor.data.GLPProcessingInfo;
import com.minexsoft.extractor.model.Fuel;
import com.minexsoft.extractor.rest.FuelService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author stainley lebron
 * @date 3/11/2017.
 */
@Stateless
public class FuelProcess {

    @Inject
    Logger logger;

    //Disable security because the page has problem with the Certificate
    static {
        disableSslVerification();
    }

    private static void disableSslVerification() {
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }


    public List<Fuel> getFuelsInformaion() {

        List<Fuel> fuelList = new ArrayList<>();
        try {
            Map<String, Fuel> fuels = new GLPProcessingInfo().extractInfoWebSite();

            for (Fuel fuel : fuels.values()) {
                fuelList.add(fuel);
            }
        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
        return fuelList;
    }

}
