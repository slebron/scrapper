package com.minexsoft.extractor.security;


/**
 * @author Stainley Lebron
 * @since 5/31/17.
 */
public enum TypeRoles {

    ADMIN, USER
}
