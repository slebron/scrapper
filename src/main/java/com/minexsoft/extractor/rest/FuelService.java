package com.minexsoft.extractor.rest;

import com.minexsoft.extractor.data.MongoDBHelper;
import com.minexsoft.extractor.model.Fuel;
import com.minexsoft.extractor.security.TypeRoles;
import com.minexsoft.extractor.service.FuelProcess;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * @author stainley lebron
 * @date 3/11/2017.
 */

@Path("/fuelservice")
@Singleton
public class FuelService {

    @Resource
    private SessionContext sessionContext;

    //@Inject
    //FuelProcess fuelProcess;

    @PostConstruct
    public void init(){
        System.out.println("Init process...");
    }

    @GET
    @Produces("application/json")
    public List<Fuel> getFuelsInformaion() {

        //List<Fuel> fuels = fuelProcess.getFuelsInformaion();
        FuelProcess fuelProcess = new FuelProcess();
        List<Fuel> fuels = fuelProcess.getFuelsInformaion();

        new MongoDBHelper().connectDatabase(fuels);
        return fuels;
    }

    @Schedule(minute = "3", hour = "15", dayOfWeek = "Fri", persistent = false)
    public void setTaskToRun(){
        this.getFuelsInformaion();
    }

}