package com.minexsoft.extractor.data;

import com.minexsoft.extractor.model.Fuel;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.BSONObject;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Stainley Lebron
 * @since 8/26/17.
 */
public class MongoDBHelper {

    public void connectDatabase(List<Fuel> fuel){
        try {

            List<BSONObject> arrayFuels = new ArrayList<>();

            String URL = "mongodb://stainley:C1c12000*@cluster0-shard-00-00-giauz.mongodb.net:27017,cluster0-shard-00-01-giauz.mongodb.net:27017,cluster0-shard-00-02-giauz.mongodb.net:27017/fuel?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";

            MongoClientURI uri = new MongoClientURI(URL);
            try (MongoClient mongoClient = new MongoClient(uri)) {
                MongoDatabase db = mongoClient.getDatabase("fuel");
                MongoCollection<Document> collection = db.getCollection("fuelprices");

                Document document = new Document();
                List<Object> fuelList = new BasicDBList();
                DBObject dbObject = new BasicDBObject();

                for (Fuel f : fuel) {
                    dbObject.put(f.getName(), f.getValue());
                }
                document.append("effective", fuel.iterator().next().getDateToUpdate());
                document.append("prices", dbObject);

                document.append("updated", new Date());

                collection.insertOne(document);

                mongoClient.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
