package com.minexsoft.extractor.data;

import com.minexsoft.extractor.model.Fuel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.minexsoft.extractor.util.GLP.*;

/**
 * @author stainley lebron
 * @date 3/11/2017.
 */
public class GLPProcessingInfo {

    private URL url;
    private String[] glps = {GAS_DIESEL.getValue(), GAS_NATURAL.getValue(), GAS_PREMIUM.getValue(), GAS_PROPANO.getValue(), GAS_DIESEL_REG.getValue(), GAS_REGULAR.getValue()};

    public Map<String, Fuel> extractInfoWebSite() throws IOException {
        Map<String, Fuel> glpPrices = new HashMap<>();

        Document document = Jsoup.connect("https://www.mic.gob.do").get();
        List<Node> element = document.getElementById("art-data-table").childNodes();
        Elements elementDate = (document.getElementsByClass("moduletable uk-precios"));
        String nodeDate = elementDate.get(0).childNodes().get(1).toString();
        String table2 = element.get(1).toString().replace("<tbody>", "").trim().replace("</tbody>", "").trim();

        String nodeDateFormated = nodeDate.replace("<h2>", "").replace("</h2>","").trim();

        String[] prices = table2.replace("\n", "").split("</tr>");

        for (int y = 0; y < prices.length; y++) {
            prices[y].trim().replace("<tr>", "").replace("</tr>", "").trim();
            String[] divided = prices[y].split("</td");

            String title = divided[0].replace("<tr>", "").replace("<td>", "").trim();
            String value = divided[1].replace(">", "").replace("<td", "").replace("RD$", "").trim();

            //Don't save Kerosene to the Database
            if (title.contains("Kerosene")) {
                continue;
            }
            if (title.contains("Gasolina Premium")) {
                title = GAS_PREMIUM.getValue();
            } else if (title.contains("Gasolina Regular")) {
                title = GAS_REGULAR.getValue();
            } else if (title.contains("Gas Licuado de Petróleo (GLP)")) {
                title = GAS_PROPANO.getValue();
            } else if (title.contains("Gas Natural Vehicular (GNV)")) {
                title = GAS_NATURAL.getValue();
            } else if (title.contains("Gasoil Optimo")) {
                title = GAS_DIESEL.getValue();
            } else if (title.contains("Gasoil Regular")) {
                title = GAS_DIESEL_REG.getValue();
            }
            glpPrices.put(title, new Fuel(title, value, nodeDateFormated));
        }
        return glpPrices;
    }

    /**
     * This method extract the information from RSS Feed MIC
     * @return
     * @throws IOException
     */
    /*public Map<String, Fuel> extractInfoRRSFeed() throws IOException {
        url = new URL("https://www.mic.gob.do/combustibleRSS.xml");
        Map<String, Fuel> glpPrices = new HashMap<>();

        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();

        InputSource source = new InputSource(is);
        SyndFeedInput input = new SyndFeedInput();
        try {
            SyndFeed feed = input.build(source);

            if (feed != null) {
                //Get all precio Combustible
                List lst = feed.getEntries();
                while (lst.iterator().hasNext()) {
                    String values = ((SyndEntryImpl) lst.get(0)).getDescription().getValue();
                    String[] newString = values.replace("<div><strong>", "").replace("</strong>", "").replace("</div>", "").trim().split("\n");

                    for (String str : newString) {
                        if (str.equals("")) {
                            continue;
                        }
                        str.replace("\n", "").trim();
                        if (!str.trim().equals("")) {
                            String[] arrValue = str.trim().split(":");
                            String title = arrValue[0];
                            String value = arrValue[1].replace("RD$", "");

                            //Don't save Kerosene to the Database
                            if (title.contains("Kerosene")) {
                                continue;
                            }

                            if (title.contains("Gasolina Premium")) {
                                title = GAS_PREMIUM.getValue();
                            } else if (title.contains("Gasolina Regular")) {
                                title = GAS_REGULAR.getValue();
                            } else if (title.contains("Gas Licuado de Petróleo (GLP)")) {
                                title = GAS_PROPANO.getValue();
                            } else if (title.contains("Gas Natural Vehicular (GNV)")) {
                                title = GAS_NATURAL.getValue();
                            } else if (title.contains("Gasoil Optimo")) {
                                title = GAS_DIESEL.getValue();
                            } else if (title.contains("Gasoil Regular")) {
                                title = GAS_DIESEL_REG.getValue();
                            }
                            glpPrices.put(title, new Fuel(title, value));
                        }
                    }
                    break;
                }
            }
        } catch (FeedException e) {
            e.printStackTrace();
        }
        return glpPrices;
    }*/
}